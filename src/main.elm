module Main exposing (Msg(..), main, update, view)

import Browser
import Debug
import Dict exposing (Dict)
import Element exposing (..)
import Element.Background as Background
import Element.Font as Font
import Element.Input as Input
import Element.Lazy
import Html exposing (Html)
import Result exposing (Result)


type alias Model =
    { inputtext : String
    , grammar : CFG
    }


type alias CFG =
    Dict String (List String)


empty : CFG
empty =
    Dict.empty


default : Model
default =
    { inputtext = ""
    , grammar =
        Dict.fromList
            [ ( "S", [ "Dit is een #schaap#" ] )
            , ( "schaap", [ "zin", "verhaal", "tekening" ] )
            ]
    }


main =
    Browser.sandbox { init = default, update = update, view = view }


type Msg
    = AddProduction String String
    | TextChange String


addProduction : String -> String -> CFG -> Result () CFG
addProduction terminal production grammar =
    case Dict.get terminal grammar of
        Just productions ->
            Ok <| Dict.insert terminal (productions ++ [ production ]) grammar

        Nothing ->
            Ok <| Dict.insert terminal [ production ] grammar



-- Dict.insert terminal [ production ] grammar


addTerminal : String -> CFG -> CFG
addTerminal terminal grammar =
    Dict.insert terminal [] grammar



-- if Dict.member terminal grammar then
--     let
--         oldProductions = Dict.get terminal grammer
--     in


update : Msg -> Model -> Model
update msg model =
    case msg of
        AddProduction terminal production ->
            { model | grammar = Result.withDefault model.grammar <| addProduction terminal production model.grammar }

        TextChange text ->
            { model | inputtext = text }



-- RemoveRule terminal ->
--     Dict.remove terminal model
-- showRules : String -> List String -> List (Html msg) -> List (Html msg)
-- showRules key value current =
--     List.append current [ div [] [ text <| key ++ " " ++ Debug.toString value ] ]


renderProduction : String -> List String -> List (Element Msg) -> List (Element Msg)
renderProduction terminal productions current =
    List.append current
        [ row [ centerX, padding 5 ]
            [ el [] <| text terminal
            , el [] <| text " → "
            , el [] <| text (Debug.toString productions)
            ]
        ]


view : Model -> Html Msg
view model =
    Element.layout
        [ Background.color (rgba 0 0 0 1)
        , Font.color (rgba 1 1 1 1)
        , Font.italic
        , Font.size 32
        , Font.family
            [ Font.external
                { url = "https://fonts.googleapis.com/css?family=EB+Garamond"
                , name = "EB Garamond"
                }
            , Font.sansSerif
            ]
        ]
    <|
        el
            [ centerX, centerY ]
        <|
            column [ centerX ] <|
                List.append (Dict.foldl renderProduction [] model.grammar)
                    [ row [ centerX ]
                        [ Input.text [ Background.color (rgb 0 0 0) ]
                            { label = Input.labelLeft [ centerY, padding 5 ] <| text "Hahah"
                            , placeholder = Nothing
                            , text = model.inputtext
                            , onChange = TextChange
                            }
                        ]
                    ]



-- ++ [ row [ centerX ]
--         [ Input.button []
--             { onPress = Just (AddProduction "S" "HAHA")
--             , label = text "Addrule S → HAHA"
--             }
--         ]
--    ]
