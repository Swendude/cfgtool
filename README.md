# CFG Tool
Trying to build a Context free grammar tool in Elm to use in teaching. Inspired by [Cheap Bots, Done Quick](https://cheapbotsdonequick.com/).

## Running
```shell
    elm install
    elm reactor
```

## TODO
* Apply productions to generate sentences
* Prevent endless recursions
* Create a message board
* Have a semi-functional UI
